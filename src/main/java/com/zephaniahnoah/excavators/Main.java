package com.zephaniahnoah.excavators;

import java.util.HashMap;
import java.util.Map;

import com.mojang.datafixers.util.Pair;
import com.zephaniahnoah.ezmodlib.EzModLib;
import com.zephaniahnoah.ezmodlib.recipe.ShapedRecipe;
import com.zephaniahnoah.ezmodlib.recipe.SmithingRecipe;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item.Properties;
import net.minecraft.item.Item;
import net.minecraft.item.ItemTier;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod(Main.MODID)
public class Main {
	public static final String MODID = "zephsexcavators";
	public static Map<PlayerEntity, Pair<Direction, BlockPos>> clickedBlockFaces = new HashMap<PlayerEntity, Pair<Direction, BlockPos>>();

	public Main() {
		EzModLib.init(MODID);
		MinecraftForge.EVENT_BUS.register(this);
		Excavator h;
		ShapedRecipe recipe;
		for (ItemTier t : ItemTier.values()) {
			Properties prop = new Item.Properties();
			if (t == ItemTier.NETHERITE)
				prop.fireResistant();
			h = new Excavator(t, prop);
			if (t == ItemTier.NETHERITE) {
				new SmithingRecipe(new ResourceLocation("netherite_excavator_recipe"), MODID + ":diamond_excavator", "minecraft:netherite_ingot", MODID + ":netherite_excavator");
				continue;
			}
			recipe = getRecipe(new ResourceLocation(t.name().toLowerCase() + "_excavator_recipe"), h);
			switch (t) {
			case WOOD:
				recipe.tag('#', "minecraft:planks");
				break;
			case STONE:
				recipe.tag('#', "forge:stone");
				recipe.tag('#', "forge:cobblestone");
				break;
			case DIAMOND:
				recipe.tag('#', "forge:gems/diamond");
				break;
			default:
				recipe.tag('#', "forge:ingots/" + h.enumName);
				break;
			}
		}
		for (Material m : Material.values()) {
			h = new Excavator(m);
			recipe = getRecipe(new ResourceLocation(m.metal + "_excavator_recipe"), h);
			recipe.tag('#', m.tag);
		}
	}

	private ShapedRecipe getRecipe(ResourceLocation rl, Excavator result) {
		ShapedRecipe recipe = new ShapedRecipe(rl, new String[] { //
				" ##", //
				" ##", //
				"/  " }, result, 1);
		recipe.item('/', "minecraft:stick");
		return recipe;
	}

	@SubscribeEvent
	public void onBreakBlock(final PlayerInteractEvent.LeftClickBlock e) {
		clickedBlockFaces.put(e.getPlayer(), new Pair<Direction, BlockPos>(e.getFace(), e.getPos()));
	}
}
